#![feature(test)]

#[cfg(test)]
mod test;

use std::fmt::Write;
use std::convert::TryInto;

static OPTIMIZE_FAST_PATH: bool = true;
static PARALLELIZATION_CHUNKS: usize = 8;

/// Given a string, a character and number, return the string extended by the an ASCII
/// representation of the number and the character
///
/// (Helper for plain_iter and partial_iter)
fn extend(acc: (String, char, usize)) -> String {
    let (mut s, c, n) = acc;
    if n > 0 {
        if OPTIMIZE_FAST_PATH && n < 10 {
            s.push((0x30 + n as u32).try_into().expect("All possible n result in legal Unicode characters"));
            s.push(c);
        } else {
            write!(s, "{}", n).expect("Writing to strings is infallible");
            s.push(c);
        }
    }
    s
}

/// Straight-forward iterater-based implementation
fn plain_iter(input: &str) -> String {
    let acc = input.chars().fold(
        (String::new(), '\0', 0),
        |acc, x| match acc {
            (s, c, n) if c == x => (s, c, n + 1),
            acc => (extend(acc), x, 1)
        }
    );
    extend(acc)
}

/// Straight-forward for-loop implementation
fn plain_loop(input: &str) -> String {
    let mut s = String::new();

    fn flush(s: &mut String, last: Option<char>, count: usize) {
        if let Some(last) = last {
            write!(s, "{}", count).expect("Writing to strings is infallible");
            s.push(last);
        }
    }

    let mut last = None;
    let mut count = 0;

    for x in input.chars() {
        if Some(x) == last {
            count += 1;
        } else {
            flush(&mut s, last, count);
            last = Some(x);
            count = 1;
        }
    }
    flush(&mut s, last, count);

    s
}

/// Jakob's implementation
fn jakob_loop(numbers: &str) -> String {
    let mut i: usize = 0;
    let mut cur_char: char = '\x00';
    let mut result = String::new();

    for n in numbers.chars() {
        if n != cur_char {
            if i > 0 {
                write!(result, "{}", i).unwrap();
                result.push(cur_char);
            }
    
            cur_char = n;
            i = 0;
        }
    
        i += 1;
    }
    
    write!(result, "{}", i).unwrap();
    result.push(cur_char);

    result
}

/// Variation of straight_iter that returns a tuple-string-tuple version suitable for
/// parallelization
fn partial_iter(input: &str) -> (char, usize, String, char, usize) {
    let initial = input.chars().next().unwrap_or('\0');

    input.chars().fold(
        (initial, 0, String::new(), '\0', 0),
        |acc, x| match acc {
            (i, n_i, s, _, 0) if x == initial => (i, n_i + 1, s, '\0', 0),
            (i, n_i, s, _, 0) => (i, n_i, s, x, 1),
            (i, n_i, s, c, n) if c == x => (i, n_i, s, c, n + 1),
            (i, n_i, s, c, n) => (i, n_i, extend((s, c, n)), x, 1)
        }
    )
    // No trailing extend as the tail may merge into the subsequent slice
}

fn merge_partial_iters<'a>(input: impl Iterator<Item=&'a (char, usize, String, char, usize)>) -> String {
    extend(input.fold(
        (String::new(), '\0', 0),
        |(mut s, mut c, mut n), x| {
            // Split head off item
            let (i, n_i, inner_s, inner_c, inner_n) = x;

            // Process head
            if *i == c {
                n += *n_i;
            } else {
                s = extend((s, c, n));
                c = *i;
                n = *n_i;
            }

            if *inner_n != 0 {
                // Only then, inner_s is potentially non-empty at all
                s = extend((s, c, n));
                s.push_str(inner_s.as_str());
                c = *inner_c;
                n = *inner_n;
            }

            (s, c, n)
        }
    ))
}

/// Split a string into PARALLELIZATION_CHUNKS roughly equal-sized chunks
///
/// Right now it relies on good luck or ASCII characters to make the chunking; is_char_boundary
/// could be used to do that better.
///
/// Helper for all parallelization runners
fn chunk(mut input: &str) -> Vec<&str> {
    let size = input.len() / PARALLELIZATION_CHUNKS;
    let mut parts = Vec::with_capacity(PARALLELIZATION_CHUNKS);
    for _ in 0..PARALLELIZATION_CHUNKS {
        let (start, end) = input.split_at(size);
        parts.push(start);
        input = end;
    }
    parts.push(input);
    parts
}

fn unparallelized(input: &str) -> String {
    // Collecting into a Vec is probably unnecessary, but was the quickest thing I could sketch
    // together that works similarly on iterator and rayon
    let collected: Vec<_> = chunk(input).iter().map(|x| partial_iter(*x)).collect();
    merge_partial_iters(collected.iter())
}

fn parallelized(input: &str) -> String {
    use rayon::prelude::*;

    let collected: Vec<_> = chunk(input).par_iter().map(|x| partial_iter(*x)).collect();
    merge_partial_iters(collected.iter())
}

fn main() {
    println!("Please use `cargo test` and `cargo bench`");
}
