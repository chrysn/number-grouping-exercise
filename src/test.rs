static SHORT: (&str, &str) = ("12344420", "111213341210");

static LONG: &str = "021222222222222222229823744444444444293874292389278234912837233333387777239212394712309458712300:-)";

extern crate test;

#[test]
fn test_plain_iter() {
    assert_eq!(super::plain_iter(SHORT.0), SHORT.1);
}

#[test]
fn test_plain_loop() {
    assert_eq!(super::plain_loop(SHORT.0), SHORT.1);
}

#[test]
fn test_jakob_loop() {
    assert_eq!(super::jakob_loop(SHORT.0), SHORT.1);
}

#[test]
fn test_unparallelized() {
    assert_eq!(super::unparallelized(LONG), super::plain_loop(LONG));
}

#[test]
fn test_partial_iter() {
    assert_eq!(super::partial_iter("000111222"), ('0', 3, "31".to_string(), '2', 3));
}


// output string is 1.3MB long
fn build_long() -> String {
    (0..10000).into_iter().map(|_| LONG).flat_map(|s| s.chars()).collect()
}

#[bench]
fn bench_plain_iter(b: &mut test::Bencher) {
    b.iter(|| super::plain_iter(build_long().as_str()));
}

#[bench]
fn bench_plain_loop(b: &mut test::Bencher) {
    b.iter(|| super::plain_loop(build_long().as_str()));
}

#[bench]
fn bench_jakob_loop(b: &mut test::Bencher) {
    b.iter(|| super::jakob_loop(build_long().as_str()));
}

#[bench]
fn bench_unparallelized(b: &mut test::Bencher) {
    b.iter(|| super::unparallelized(build_long().as_str()));
}

#[bench]
fn bench_parallelized(b: &mut test::Bencher) {
    b.iter(|| super::parallelized(build_long().as_str()));
}
